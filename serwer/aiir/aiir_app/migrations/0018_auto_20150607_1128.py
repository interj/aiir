# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aiir_app', '0017_auto_20150604_1348'),
    ]

    operations = [
        migrations.AddField(
            model_name='pending',
            name='progress_current',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='pending',
            name='progress_max',
            field=models.IntegerField(default=12),
        ),
    ]
