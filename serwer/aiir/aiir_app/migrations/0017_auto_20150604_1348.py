# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aiir_app', '0016_pending_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pending',
            old_name='user',
            new_name='current_user',
        ),
        migrations.AddField(
            model_name='done',
            name='current_user',
            field=models.TextField(default=b'x'),
        ),
    ]
