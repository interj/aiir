from django.conf.urls import url
from . import views

urlpatterns = [

    #url(r'^$', views.index),
    url(r'^tasks/$', views.get_task, name='tasks'),
    url(r'^register/$', views.register, name='register'),
    url(r'^$', views.home, name='home'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^tasks/progress/$', views.get_progress, name='progress'),
    url(r'^tasks/pending/$', views.get_pending, name='getpending'),
    url(r'^tasks/done/$', views.get_done, name='getdone'),
    url(r'^tasks/addtask/$', views.post_task, name='addtask'),
]