from django.shortcuts import render, render_to_response
from django.http import HttpResponse, Http404, HttpResponseRedirect, JsonResponse
from django.template import RequestContext, loader
from .models import Done, Pending
from .forms import RequestForm, TaskForm, UserForm, LoginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
def home(request):
    return render(request, 'home.html')

@login_required
def user_logout(request):
    logout(request)
    #return HttpResponseRedirect('/index/')
    return render(request, 'home.html')

def user_login(request):
    context = RequestContext(request)
    if request.method == 'POST':

        login_form = LoginForm(data=request.POST)
        if login_form.is_valid():

            username = request.POST['username']
            password = request.POST['password']

            user = authenticate(username=username, password=password)

            if user:
                if user.is_active:
                    login(request, user) 
                    return HttpResponseRedirect('/tasks/')
                #return render(request, 'aiir_app/tasks.html', {})
                else:
                    return HttpResponse("Twoje konto jest nieaktywne.")
            else:
                print "Nieprawidlowe dane konta: {0}, {1}".format(username, password)
                return render_to_response('login.html', {'login_form':login_form, 'logerr':True}, context)
        else:
            print login_form.errors
    else:
        login_form = LoginForm()

    return render_to_response('login.html', {'login_form':login_form}, context)

def register(request):
    context = RequestContext(request)

    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            registered = True

            user = authenticate(username=request.POST['username'], 
                                password=request.POST['password'])
            login(request, user)
            return HttpResponseRedirect('/tasks/')
        else:
            print user_form.errors
    else:
        user_form = UserForm()

    return render_to_response(
            'register.html',
            {'user_form': user_form, 'registered': registered},
            context)

def index(request):
	return render_to_response('index.html', {'user':request.user})

@login_required
def get_task(request):
	current_result = []
	lista = Pending.objects.filter(current_user=str(request.user.username))
	msg = ''
	all_result = Done.objects.filter(current_user=str(request.user.username))

	form = TaskForm()

	return render(request, 'tasks.html', {'form': form, 'msg': msg, 'current_result': current_result, 'lista': lista, 'all_result': all_result})

@login_required
def post_task(request):
    
    msg = ''

    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            temp = request.POST['range_of']

            try:
                current_result = Done.objects.get(rangeof=temp, current_user=str(request.user.username))
                all_result = Done.objects.all()
                msg = "Ilosc liczb pierwszych dla zadanego przedzialu: "+ str(current_result.result)
            except Done.DoesNotExist:
                try:
                    x = Pending.objects.get(rangeof=temp, current_user=str(request.user.username))
                    msg = "Takie zadanie oczekuje juz w kolejce."
                except Pending.DoesNotExist:
                    Pending.objects.create(rangeof=temp,current_user=str(request.user.username))
                    msg = "Twoje zadanie zostalo dodane do kolejki." 
    
        
    jsonObj = {"msg":msg}

    return JsonResponse(jsonObj)


@login_required
def get_progress(request):

    lista = Pending.objects.filter(current_user=str(request.user.username))
    
    jsonArray = {}
    json = []
    for e in lista:
        jsonObj = {"id":e.id, "progress_max":e.progress_max, "progress_current":e.progress_current}
        json.append(jsonObj)

    jsonArray['progress']=json

    return JsonResponse(jsonArray)

@login_required
def get_pending(request):

    lista = Pending.objects.filter(current_user=str(request.user.username))

    jsonArray = {}
    json = []
    for e in lista:
        jsonObj = {"id":e.id, "priority":e.priority, "rangeof":e.rangeof, "request_time":e.request_time, "progress_max":e.progress_max, "progress_current":e.progress_current}
        json.append(jsonObj)

    jsonArray['pending']=json

    return JsonResponse(jsonArray)

@login_required
def get_done(request):

    lista = Done.objects.filter(current_user=str(request.user.username))

    jsonArray = {}
    json = []
    for e in lista:
        jsonObj = {"id":e.id, "rangeof":e.rangeof, "result":e.result, "current_user":e.current_user}
        json.append(jsonObj)

    jsonArray['done']=json

    return JsonResponse(jsonArray)











