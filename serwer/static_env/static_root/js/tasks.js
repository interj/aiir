
$(document).ready(function()
{

	setInterval(update_progress, 1000);

	$('#id_button').click(function(e){
		e.preventDefault();
		post_task();
		update_pending();
	});


});


function update_progress(){

	var current_prog = 0;

	$.get( "progress/", function(data){
		
		$.each(data.progress, function(i, obj){

			current_prog = (obj.progress_current/obj.progress_max) * 100;

			$('#'+obj.id).css('width', current_prog+'%').attr('aria-valuenow', current_prog);
			
		});

	}, "json");

}

function update_pending(){

	$.get( "pending/", function(data){

		var table = $('#pending');
		table.html('');
		$.each(data.pending, function(i, obj){

			current_prog = (obj.progress_current/obj.progress_max) * 100;

			table.append('<tr><td>'+obj.rangeof+'</td><td>'+obj.priority+'</td><td><div class="progress"><div id="'+obj.id+'" class="progress-bar" role="progressbar" aria-valuenow="'+current_prog+'" aria-valuemin="0" aria-valuemax="100" style="width:'+current_prog+'%;"></div></div></td></tr>');
			
		});

	}, "json");
}

function update_done(){

	$.get( "done/", function(data){

		var table = $('#done');
		table.html('');
		$.each(data.pending, function(i, obj){

			table.append('<tr><td>'+obj.rangeof+'</td><td>'+obj.result+'</td><td>'+obj.current_user+'</td></tr>');
			
		});

	}, "json");

}

function post_task(){

	var range = $('#id_range_of');
	var text = $('#id_msg');

	$.ajax({
		type: "POST",
		url: "addtask/",
		dataType: "json",
		data: {
			'range_of' : range.val(),
			'csrfmiddlewaretoken' : $("input[name=csrfmiddlewaretoken]").val()
		},

		success: function(data){
			text.html('');
			var msg = data.msg;
			text.append('<p>'+msg+'</p>');
		},


	});


}

