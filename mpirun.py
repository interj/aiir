#!/usr/bin/python3
# -*- coding: utf-8 -*-
from time import sleep
from subprocess import Popen, PIPE
import tasks

tm = tasks.TaskManager('serwer/aiir/db.sqlite3')

np_max = 11

while True:
    
    item = tm.get_high_priority_item()
    if not item:
        print( '--- Brak zadan w kolejce... Czekam... ---')
        sleep(10)
        continue
    
    else:
        np = 2 if item.rangeof < 500 else np_max
        
        process = Popen(
            # open mpi
            #['mpirun', '--hostfile', './hosts', '--launch-agent', 'ssh', '-np', '{}'.format(np), '-q', '--bind-to', 'hwthread:overload-allowed', 'slave/slave', str(item.rangeof) ],
            # mpich
            ['mpirun', '-f', './hosts', '--launcher', 'ssh', '-np', '{}'.format(np), '--bind-to', 'hwthread', 'slave/slave', str(item.rangeof)],
            stdout=PIPE, stderr=PIPE,
            universal_newlines=True)

        print( '--- mpirun -np {} ---'.format(np))
        print( item)

    for progress in process.stderr:
        #item.progress_current = progress
        tm.update_progress(item.id, int(progress))

    result = process.communicate()[0]
    
    print( '--- Result: {} ---'.format(result))
    
    tm.add_done_item(item.rangeof, int(result), item.current_user)
    sleep(0.1)
    tm.rm_pending_item(item.id)
    sleep(0.1)
