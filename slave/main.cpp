#include <iostream>
#include <boost/mpi.hpp>
#include "Worker.hpp"

void handleSmallProblems(long size)
{
    size += 1;
    size *= 2;
    if(size > 10)
        return;
    else if(size < 2)
        std::cout << 0 << std::endl;
    else if(size < 3)
        std::cout << 1 << std::endl;
    else if(size < 5)
        std::cout << 2 << std::endl;
    else if(size < 7)
        std::cout << 3 << std::endl;
    else
        std::cout << 4 << std::endl;
    std::cerr << 100 << std::endl;
    exit(0);
}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        std::cerr << "Binary takes one and only one additional argument: size of range to seek for primes" << std::endl;
        return -1;
    }
    boost::mpi::environment env(argc, argv);
    boost::mpi::communicator world;
    const long problemSize = std::ceil(std::stol(argv[1]) / 2.0) - 1;
    if(world.rank() > 0)
    {
        handleSmallProblems(problemSize);
    }

    auto workersComm = world.split(world.rank() && true);
    if(world.rank() > 0)
    {
        auto& worker = Worker::getInstance(problemSize, workersComm);
        worker.run();
    }
    if(world.rank() == 0)
    {
        short progressPercentage = 0;
        while (progressPercentage != 100)
        {
            const short maxProgress = 100;
            short const & (*min) (short const &, short const &) = std::min<short>;
            boost::mpi::reduce(world, maxProgress, progressPercentage, min, 0);
            std::cerr << progressPercentage << std::endl;
        }
        long sumOfPrimes = 1; //account for earlier popped 2
        long output;
        boost::mpi::reduce(world, sumOfPrimes, output, std::plus<>(), 0);
        std::cout << output << std::endl;
    }
    return 0;
}
