from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class TaskPending(Base):
    __tablename__ = 'aiir_app_pending'

    id = Column(Integer, primary_key=True)
    rangeof = Column(Integer)
    request_time = Column(DateTime)
    priority = Column(Integer)
    current_user = Column(String)
    progress_current = Column(Integer)
    progress_max = Column(Integer)

    def __repr__(self):
        return 'id:{}, rangeof:{}, request_time:{}, priority:{}, current_user:{}, progress_current:{}, progress_max:{}'.format(self.id, self.rangeof, self.request_time, self.priority, self.current_user, self.progress_current, self.progress_max)


class TaskDone(Base):
    __tablename__ = 'aiir_app_done'

    id = Column(Integer, primary_key=True)
    rangeof = Column(Integer)
    result = Column(Integer)
    current_user = Column(String)

    def __init__(self, rangeof, result, current_user):
        self.rangeof = rangeof
        self.result = result
        self.current_user = current_user


class TaskManager():
    def __init__(self, dbname):
        self.dbname = dbname
        self.dbeng = create_engine('sqlite:///{}'.format(self.dbname))

        Base.metadata.create_all(self.dbeng)
        self.Session = sessionmaker(bind=self.dbeng)
        self.session = self.Session()

    def commit(self):
        try:
            self.session.commit()
        except:
            self.session.rollback()

    def add_done_item(self, rangeof, result, current_user):
        self.session.add(TaskDone(rangeof, result, current_user))
        self.commit()

    def get_high_priority_item(self):
        return self.session.query(TaskPending).order_by(TaskPending.priority.desc()).first()

    def rm_pending_item(self, id):
        self.session.query(TaskPending).filter(TaskPending.id == id).delete()
        self.commit()

    def update_progress(self, id, progress):
        self.session.query(TaskPending).filter(TaskPending.id == id).update({TaskPending.progress_current: progress})
        self.commit()

# debug
if __name__ == '__main__':
    
    tm = TaskManager('db.sqlite3')
    
    print( '--- Pobieranie high_priority_item ---')
    high_item = tm.get_high_priority_item()
    if high_item:
        print( high_item)
    else:
        print( 'Brak :)')
    
    print( '--- Dodawanie do _done ---')
    result = 1234
    tm.add_done_item(high_item.rangeof, result, high_item.current_user)
    
    print( '--- Usuwanie z _pending id {} ---'.format(high_item.id))
    tm.rm_pending_item(high_item.id)

    print( '--- Pobieranie nowego high_priority_item ---')
    high_item = tm.get_high_priority_item()
    print( high_item)

    print( '--- Zmiana statusu ---')
    tm.update_progress(high_item.id, 100)

    print( high_item)
